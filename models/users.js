'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;


let users = new Schema({
  user : String,
  password : String,
  role: {
    type: String,
    enum: ['admin', 'normal']
  },
  joinDate: { type: Date, default: Date.now }
});

module.exports = mongoose.model('users', users);
