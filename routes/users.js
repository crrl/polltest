let express = require('express');
let router = express.Router();
let jwt    = require('jsonwebtoken');
let app = require('../app');
let Users = require('../models/users');

router.post('/api/authenticate', (req, res) => {
  Users.findOne({
    user: req.body.username
  }, function (err, user) {
    if (err) {
      throw err;
    }
    if (!user) {
      res.status(400).end('No hay usuarios con ese Nombre/Contraseña');
    } else if (user) {
      let tempPass = Buffer.from(req.body.password, 'base64').toString();
      
      if (user.password !== tempPass) {
        res.status(400).end('No hay usuarios con ese Nombre/Contraseña');
      } else {
        let payload = {
          role: user.role,
          _id: user._id
        };
        let token = jwt.sign(payload, app.get('secretKey'), {
          expiresIn: 31536
        });
        res.send({
          status: 200,
          result: {
            token,
            _id: user._id,
            user: user.user,
            role: user.role
          }
        });
      }
    }
  });
});

router.get('/api/verifyUser/:id', (req, res) => {
    Users.findById({
      _id: req.token._id
    }, function (err, user) {
      if (err) {
        res.status(404);
        res.end('Ha ocurrido un error.');
      }
      if (!user) {
        res.status(404);
        res.end('Ha ocurrido un error.');
      } else {
        res.send({
          status:200,
          result: {
          role: user.role          
          }
        });
      }
    });
});

router.get('/api/test', (req, res) => {
  Users.find((err, result) => {
    if (err) {
      res.write(err);
    } else {
      res.send({
        status:200,
        result
      });
    }
  });
});

module.exports = router;
