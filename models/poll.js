'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;


let polls = new Schema({
  name : String,
  options : [{}],
  usersVoted: [String],
  creationDate: { type: Date, default: Date.now }
});

module.exports = mongoose.model('polls', polls);
