FROM node:12.10.0-alpine

WORKDIR /usr/src/app

COPY . .

RUN apk add --no-cache tzdata

RUN npm install &&  npm -g install nodemon

EXPOSE 4201

CMD ["nodemon", "bin/www"]
