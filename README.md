# Datank api



```bash
sudo apt-get install -y docker.io

sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

docker-compose up -d
```


# Users

```

    "user": "user1",
    "password": "user1",
    "role": "normal"

    "user": "user2",
    "password": "user2",
    "role": "normal"

    "user": "user3",
    "password": "user3",
    "role": "normal"

    "user": "user4",
    "password": "user4",
    "role": "normal"
 
    "user": "user4",
    "password": "user4",
    "role": "normal"
 
    "user": "admin1",
    "password": "admin1",
    "role": "admin"
 
    "user": "admin2",
    "password": "admin2",
    "role": "admin"
  
    "user": "admin3",
    "password": "admin3",
    "role": "admin"
 
    "user": "admin4",
    "password": "admin4",
    "role": "admin"
  
    "user": "admin4",
    "password": "admin4",
    "role": "admin"
  
```