let express = require('express');
let router = express.Router();
let Polls = require('../models/poll');

router.post('/api/poll', (req, res) => {
  req.body.pollItems.forEach((element, index) => {
    element.id = index;
    element.userId = [];
  });
  
  let poll = new Polls({
    name : req.body.pollName,
    options: req.body.pollItems,
    usersVoted: []
  });

  poll.save((err, result) => {
    if (err) {
      res.write(err);
    } else {
      res.send({status:200});
    }
  });
});

router.get('/api/poll', (req, res) => {
  Polls.find((err, result) => {
    if (err) {
      res.write(err);
    } else {
      let polls = [];
      result.forEach((poll) => {
        let answered = false;
        if (poll.usersVoted.indexOf(req.token._id) >= 0) {
          answered = true;
        }
        polls.push({
          name: poll.name,
          _id: poll._id,
          answered
        });
      });
      res.send({
        status:200,
        result: polls
      });
    }
  });
});

router.get('/api/poll/:id', (req, res) => {
  Polls.findOne({ _id: req.params.id },(err, result) => {
    if (err) {
      res.write(err);
    } else {
      res.send({
        status:200,
        result
      });
    }
  });
});

router.put('/api/poll', (req, res) => {
  Polls.findOne({_id: req.body.pollId}, (err, poll) => {
    if (err) {
      return err;
    } else {
      if (poll.usersVoted.indexOf(req.token._id) >= 0) {
        res.status(500);
        res.end('The user has already cast his vote.');
      } else {
        poll.usersVoted.push(req.token._id);   
        poll.options[req.body.selectedAnswer].userId.push(req.token._id);
        Polls.update({_id: req.body.pollId}, { $set: { "options": poll.options, "usersVoted": poll.usersVoted } }, (err, result) => {
          if (err) {
            return err;
          } else {
            res.send({
              status:200
            });
          }
        });
      } 
    }
  });
});

module.exports = router;
