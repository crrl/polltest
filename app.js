'use strict';

const createError = require('http-errors');
const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const jwt    = require('jsonwebtoken');
let mongoose = require('mongoose');
let cors = require('cors')

mongoose.connect('mongodb://mongo/datank', {
  auto_reconnect: true,
  db: {
    w: 1
  },
  server : {
    reconnectTries : Number.MAX_VALUE,
    autoReconnect : true,
    socketOptions: {
      keepAlive: 1
    }
  }
}, function (err, db) {
  if (err) {
    console.log(err);
  } else {
    console.log('Connected');
  }  
});


let app = module.exports = express();


const pollsRouter = require('./routes/polls');
const usersRouter = require('./routes/users');



app.set('secretKey', 'D4T4nK.*-');
app.use(cors());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());


app.use(function (req, res, next) {
  if (req.url === '/api/authenticate' || req.url === '/api/test') {
    next();
  } else {
    jwt.verify(req.headers.token, app.get('secretKey'), function (err, decoded) {
      if (err) {
        res.status(500).send({'access': false, 'status':500});
      } else {
        req.token = decoded;        
        next();
      }
    });
  }
});

app.use(pollsRouter);
app.use(usersRouter);
